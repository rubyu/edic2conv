# -*- coding: utf-8 -*-

'''
Created on 2013/03/06

@author: user
'''

import logging
logging.basicConfig(
    level=logging.INFO,
    format="%(levelname)-8s %(module)-16s %(funcName)-16s@%(lineno)d - %(message)s"
)

import unittest
import os
import sys
import re
import argparse
from datetime import datetime

from edic2 import EDIC
from fdt import FDTParserVisitor


class FDTParserVisitorCSVFormatter(FDTParserVisitor):
    def __init__(self, path):
        self._path = path
        self._csv = {}
        self._last_time = datetime.now()
    
    def _open(self, key, filename):
        path = os.path.join(self._path, filename)
        import csv
        self._csv[key] = csv.writer(open(path, "wb"), quoting=csv.QUOTE_ALL)
        sys.stdout.write(" - %s\n" % path)
        
    def print_progress(self, min_interval_seconds=2):
        now = datetime.now()
        if min_interval_seconds < (now - self._last_time).total_seconds():
            sys.stdout.write(".")
            self._last_time = now
    
    def _write(self, key, row):
        self.print_progress()
        self._csv[key].writerow([text.encode("utf-8") for text in row])
        

class FDTParserVisitorDumpFormatter(FDTParserVisitorCSVFormatter):
    def __init__(self, path, db_info):
        super(FDTParserVisitorDumpFormatter, self).__init__(path)
        self._open("dump", db_info.dicname + ".csv")
        self._write("dump", ["Id", "SubId", "Speech", "Key", "Index", "Comment", "Example",
                             "WordForm1", "WordForm2", "WordForm3", "WordForm4"])
    
    def visit(self, entry):
        self._write("dump", [entry.id, entry.sub_id, entry.speech, entry.key, 
                             entry.index, entry.comment, entry.example,
                             entry.word_form1, entry.word_form2, 
                             entry.word_form3, entry.word_form4])


class FDTEntryAnkiFormatter(object):
    @classmethod
    def index(cls, text):
        m = re.search(ur"(.+)? \| (([^［]+)?(［(.+)］)?)?", text)
        if not m:
            return text
        if m.group(1) != None:
            return m.group(1) 
        if m.group(5) != None:
            return m.group(5)
        if m.group(3) != None:
            return m.group(3)
        return ""
        
    @classmethod
    def comment(cls, text):
        text = re.sub(ur"▲▲(.+)?/(\d{11})△△", ur"\1", text)
        text = re.sub(ur"<CR>", "<br><br>", text)
        return text
    
    @classmethod
    def example(cls, text):
        if text.count("|") > 1:
            raise Exception()
        return [t.strip() for t in text.split("|")]
    
    
class TestFDTEntryWrapper(unittest.TestCase):
    def test_index(self):    
        self.assertEqual(FDTEntryAnkiFormatter.index(u""), u"")
        self.assertEqual(FDTEntryAnkiFormatter.index(u"A | "), u"A")
        self.assertEqual(FDTEntryAnkiFormatter.index(u" | "), u"")
        self.assertEqual(FDTEntryAnkiFormatter.index(u" | あっぷでーとする［アップデートする］"), u"アップデートする")
        self.assertEqual(FDTEntryAnkiFormatter.index(u"4C | フォーシー、ヨンシー"), u"4C")
        self.assertEqual(FDTEntryAnkiFormatter.index(u"kitty-corner | 対角線の［に］、斜め向かいの［に］"), u"kitty-corner")
        self.assertEqual(FDTEntryAnkiFormatter.index(u"losingest | 負けが最多の、最低［最下位］の、最悪の"), u"losingest")

    def test_comment(self):
        self.assertEqual(FDTEntryAnkiFormatter.comment(u"<CR>→▲▲烈火のごとく怒る/00500031090△△"), u"<br><br>→烈火のごとく怒る")
        
    def test_example(self):
        self.assertEqual(FDTEntryAnkiFormatter.example(u" a  |  b "), [u"a", u"b"])
        self.assertEqual(FDTEntryAnkiFormatter.example(u" aa  |  bb "), [u"aa", u"bb"])


class FDTParserVisitorAnkiFormatter(FDTParserVisitorCSVFormatter):
    # Index
    #   = en_desc " | " ja_desc
    #   en_desc = str+
    #   ja_desc = str+ | str+［str+］ //kana[kanji_kana]
    #
    #   優先度は en_desc, kanji_kana, kana の順
    #
    # Comment
    #   = str+
    #
    #   ▲▲link_string/link_id△△ という表現が含まれるので、これを整形する 
    #   <CR> という表現が含まれるので、これを <br><br> に置き換える
    #
    # Example
    #   = " " en_desc "  |  " ja_desc " "
    #   en_desc = str+
    #   ja_desc = str+
    #
    #   対訳として、en-ja, ja-enそれぞれを出力する
    
    def __init__(self, path, db_info):
        super(FDTParserVisitorAnkiFormatter, self).__init__(path)
        self._open("index-comment", db_info.dicname + u" 語義.csv")
        self._open("exampleAB", db_info.dicname + u" 対訳(英和).csv")
        self._open("exampleBA", db_info.dicname + u" 対訳(和英).csv")
    
    def visit(self, entry):
        index = FDTEntryAnkiFormatter.index(entry.index)
        comment = FDTEntryAnkiFormatter.comment(entry.comment) 
        example = FDTEntryAnkiFormatter.example(entry.example)
        if int(entry.sub_id) == 0:
            self._write("index-comment", [index, entry.index + "<br><br>" + comment])
        else:
            self._write("exampleAB", [example[0], example[1]])
            self._write("exampleBA", [example[1], example[0]])
    

def get_arg():
    parser = argparse.ArgumentParser(prog="edic2conv.exe",
                                     description="E-DIC v2 converter")
    parser.add_argument("--format", 
                        default="dump", 
                        choices=["dump", "anki"])
    parser.add_argument("--config", 
                        default="C:\\program files\\E-DIC2\\dicts\\config.ini",
                        help="path to config.ini")
    parser.add_argument("output", 
                        help="path to output directory")
    return parser.parse_args()

def main():
    arg = get_arg()
        
    sys.stdout.write("config: %s\n" % arg.config)
    sys.stdout.write("output: %s\n" % arg.output)
    sys.stdout.write("format: %s\n" % arg.format)
    
    if 0 != len(os.listdir(arg.output)):
        sys.stderr.write("Output directory must be empty!\n")
        return 
    
    edic = EDIC(arg.config)
    dics = edic.dictionaries()
    for dic in dics:
        sys.stdout.write("\n")
        sys.stdout.write("[%s](%s of %s)\n" % (dic.info.dicname, dics.index(dic)+1, len(dics)))
        if arg.format == "ebstudio":
            pass
        elif arg.format == "anki":
            visitor = FDTParserVisitorAnkiFormatter(arg.output, dic.info)
        elif arg.format == "dump":
            visitor = FDTParserVisitorDumpFormatter(arg.output, dic.info)
        dic.accept(visitor)
        sys.stdout.write("done.\n")
        

class TestMain(unittest.TestCase):
    def test_main(self):
        """
        import shutil
        
        if os.path.exists("test/edic2conv/anki"):
            shutil.rmtree("test/edic2conv/anki")
        os.makedirs("test/edic2conv/anki")
        sys.argv = ["", "--format", "anki", "--config", "dicts/config.ini", "test/edic2conv/anki"]
        main()
        """
        
        """
        if os.path.exists("test/edic2conv/dump"):
            shutil.rmtree("test/edic2conv/dump")
        os.makedirs("test/edic2conv/dump")
        sys.argv = ["", "--format", "dump", "--config", "dicts/config.ini", "test/edic2conv/dump"]
        main()
        """

if __name__ == "__main__":
    main()