## E-DIC英和和英第2版をCSVに変換するサンプル

### Usage

    edic2conv.exe [-h] [--format {dump,anki}] [--config CONFIG] output

    E-DIC v2 converter

    positional arguments:
      output                path to output directory

    optional arguments:
      -h, --help            show this help message and exit
      --format {dump,anki}
      --config CONFIG       path to config.ini


### すべてのデータを出力する場合
コマンドプロンプトにて、例えば

    > cd /d C:\Users\username\Downloads\edic2conv
    > mkdir out
    > edic2conv.exe out


この場合、config、formatにはデフォルト値が適用され、


    config: C:\program files\E-DIC2\dicts\config.ini
    output: out
    format: dump
    
    [E-DIC英和辞典](1 of 24)
     - out\E-DIC英和辞典.csv
    ......


のように処理が進みます。C:\Users\username\Downloads\edic2conv\outにデータが出力されます。CSVの内容は


    "Id","SubId","Speech","Key","Index","Comment","Example","WordForm1","WordForm2","WordForm3","WordForm4"
    "01500000010","000","名詞","a","A | ","《名詞》（またａで）(1)英語アルファベットの第１字 (2)Ａ，ａ字によって表される音 (3)Ａ字形のもの","","As | A's","","",""
    ...


のようになります。データは何も加工されず、そのまま出力されます。


### Ankiに適した出力


    > edic2conv.exe --format anki out


この場合、出力は

 - 語義
 - 対訳(英和)
 - 対訳(和英)

に分けて出力されます。


    config: C:\program files\E-DIC2\dicts\config.ini
    output: out
    format: anki
    
    [E-DIC英和辞典](1 of 24)
     - out\E-DIC英和辞典 語義.csv
     - out\E-DIC英和辞典 対訳(英和).csv
     - out\E-DIC英和辞典 対訳(和英).csv
    ......


#### 語義

    "ABANDON TO～略～","ABANDON TO～略～ | <br><br>「～を（あきらめて，見捨てて）～略～"

#### 対訳(英和)

    "The crew was forced to abandon～略～","乗組員たちはついに船を捨て，～略～"

#### 対訳(和英)

    "乗組員たちはついに船を捨て，～略～", "The crew was forced to abandon～略～"

のようになります。
データをそのまま出力はせず、作者のニーズに合わせて以下のような処理を行なっています。

- Indexについて、必要な物以外を切り捨て
- 例文は別に出力
- ▲▲～△△で囲まれている部分からIDを取り除く
- CRタグをBRタグに置換

注意: 「E-DIC英和辞典」「E-DIC和英辞典」「科学技術用語辞典」の対訳ファイルは出力が空（0バイト）のファイルになります。これらの辞典には対訳例文のデータが含まれていないからです。

###既知の問題
####「E-DIC 第2版 ダウンロード版」でエラーが発生する

    -英和イディオム完全対訳辞典(_19z4.inx)
    -科学技術用語辞典(_c5d1.inx)
    
の2辞書でエラーが発生する場合、「辞書増量サービス」に修正データが含まれていますので、会員登録後、ダウンロードして適用してください。
